<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Tienda Virtual</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form action="${pageContext.request.contextPath}/login" method="post">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <p>
                <label for="username">Usuario</label>
                <input type="text" id="txtUsuario" name="txtUsuario"/>
            </p>
            <p>
                <label for="password">Contraseña</label>
                <input type="password" id="txtContrasena" name="txtContrasena"/>
            </p>
            <button type="submit" class="btn">Ingresar</button>
        </form>
    </body>
</html>
