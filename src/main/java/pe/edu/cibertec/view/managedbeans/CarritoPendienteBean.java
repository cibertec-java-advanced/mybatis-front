package pe.edu.cibertec.view.managedbeans;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.FacesRequestAttributes;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import pe.edu.cibertec.dominio.Carrito;
import pe.edu.cibertec.dominio.Cliente;
import pe.edu.cibertec.dominio.DetalleCarrito;
import pe.edu.cibertec.dominio.Producto;
import pe.edu.cibertec.dominio.service.CarritoService;
import pe.edu.cibertec.repositorio.CarritoRepositorio;
import pe.edu.cibertec.repositorio.impl.CarritoMybatisRepositorioImpl;

@Component("carritoPendienteBean")
@Scope("session")
public class CarritoPendienteBean {
    private static final long serialVersionUID = 1L;
    private List<DetalleCarrito> detallesCarritos = new ArrayList<>();
    private Integer cantidadProductos = 0;
    private Double total = 0.0;
    private Producto producto;
    
    @Autowired
    private CarritoService carritoService;
    
    @PostConstruct
    public void init() {
        /*FacesRequestAttributes attr = (FacesRequestAttributes )
                RequestContextHolder.currentRequestAttributes();
        producto = (Producto)attr.getAttribute("producto", RequestAttributes.SCOPE_SESSION);
        System.out.println("Producto: " + producto.getNombre());*/
    }
    
    public void agregarProductoACarrito(Producto data) {
        Producto producto = new Producto();
        Boolean bandera = false;
        
        for(int i=0; i<detallesCarritos.size(); i++){
            if( detallesCarritos.get(i).getProducto().getId().toString().equals(data.getId().toString())){
                Integer cantidad = detallesCarritos.get(i).getCantidad() + 1;
                Double precioCalculado = Double.parseDouble(data.getPrecio().toString()) * cantidad;
                BigDecimal bigDecimal = new BigDecimal(precioCalculado, MathContext.DECIMAL64);
                                
                detallesCarritos.get(i).setCantidad(cantidad);
                detallesCarritos.get(i).setPrecioCalculado(bigDecimal);
                bandera = true;
            }
        }
        
        if(!bandera){
            producto.setId(data.getId());
            producto.setDescripcion(data.getDescripcion());
            producto.setDescripcion(data.getDescripcion());
            producto.setNombre(data.getNombre());
            producto.setMarca(data.getMarca());
            producto.setCategoria(data.getCategoria());

            DetalleCarrito detalle = new DetalleCarrito();
            detalle.setProducto(producto);
            detalle.setPrecioUnitario(data.getPrecio());
            detalle.setPrecioCalculado(data.getPrecio());
            detalle.setCantidad(1);

            detallesCarritos.add(detalle);            
        }
        
        cantidadProductos = cantidadProductos + 1; 
        total = total + Double.parseDouble(data.getPrecio().toString());
    }
    
    public void changeCantidadCarrito(DetalleCarrito data) {
        System.out.println("Cantidad: "+data.getCantidad());
        System.out.println("Producto: "+data.getProducto().getNombre());
    }
    
    public void eliminarProducto(DetalleCarrito data) {
        cantidadProductos = cantidadProductos - data.getCantidad(); 
        total = total - Double.parseDouble(data.getPrecioCalculado().toString());
        detallesCarritos.remove(data);
    }
    
    public String guardarCarrito(Cliente dataCliente, List<DetalleCarrito> dataCarrito){
        Cliente cliente = new Cliente();
        cliente.setId(dataCliente.getId());
        cliente.setNombre(dataCliente.getNombre());
        cliente.setApellidoPaterno(dataCliente.getApellidoPaterno());
        cliente.setApellidoMaterno(dataCliente.getApellidoMaterno());
        
        Carrito carrito = new Carrito();
        carrito.setCantidad(cantidadProductos);
        carrito.setCliente(cliente);
        carrito.setFechaCompra(new Date());
        BigDecimal bigDecimal = new BigDecimal(total, MathContext.DECIMAL64);
        carrito.setTotal(bigDecimal);
        
        carritoService.crearCarrito(carrito, dataCarrito);
        limpiar();
        return "productos.xhtml?faces-redirect=true"; 
    }
    
    public String verCarrito(){
        return "carrito.xhtml?faces-redirect=true";
    }
    
    public String verProducto(){
        return "productos.xhtml?faces-redirect=true";
    }
    
    public void limpiar(){
        cantidadProductos = 0;
        total = 0.0;
        detallesCarritos.clear();
    }
    
    public List<DetalleCarrito> getDetallesCarritos() {
        return detallesCarritos;
    }
    public void setDetallesCarritos(List<DetalleCarrito> detallesCarritos) {
        this.detallesCarritos = detallesCarritos;
    }
    public Integer getCantidadProductos() {
        return cantidadProductos;
    }
    public void setCantidadProductos(Integer cantidad) {
        this.cantidadProductos = cantidad;
    }
    public Double getTotal() {
        return total;
    }
    public void setTotal(Double total) {
        this.total = total;
    }    
    public Producto getProducto() {
        return producto;
    }
    public void setProducto(Producto producto) {
        this.producto = producto;
    }
}
