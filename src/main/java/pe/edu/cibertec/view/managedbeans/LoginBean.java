package pe.edu.cibertec.view.managedbeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pe.edu.cibertec.dominio.Cliente;
import pe.edu.cibertec.dominio.Usuario;
import pe.edu.cibertec.dominio.service.ClienteService;
import pe.edu.cibertec.dominio.service.UsuarioService;

@Component("loginBean")
@Scope("session")
public class LoginBean {
    private static final long serialVersionUID = 1L;
    private String usuario;
    private String clave;
    private Cliente cliente;
    
    @Autowired
    private UsuarioService usuarioService;
    
    @Autowired
    private ClienteService clienteService;
    
    public String login() {
        Usuario us = usuarioService.iniciarSesion(this.usuario, this.clave);
        if(us != null){
            this.cliente = clienteService.buscarPorUsuario(us.getId());
            this.clave = null;
        }
        return us == null ? "" : "productos.xhtml?faces-redirect=true";
    }

    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public String getClave() {
        return clave;
    }
    public void setClave(String clave) {
        this.clave = clave;
    }   
    public Cliente getCliente() {
        return cliente;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
