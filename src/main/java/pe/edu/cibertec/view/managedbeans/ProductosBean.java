package pe.edu.cibertec.view.managedbeans;

import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pe.edu.cibertec.dominio.Producto;
import pe.edu.cibertec.dominio.service.ProductoService;

@Component("productosBean")
@Scope("view")
public class ProductosBean {
    private static final long serialVersionUID = 1L;
    private List<Producto> lstProducto;
    
    @Autowired
    private ProductoService productoService;

    public ProductosBean(){
    }
    
    @PostConstruct //Tiene que ser void, public, no excepciones, no argumentos
    public void init(){     
        lstProducto = productoService.obtenerTodos();
    }

    public List<Producto> getLstProducto() {
        return lstProducto;
    }
    public void setLstProducto(List<Producto> lstProducto) {
        this.lstProducto = lstProducto;
    }
}
