package pe.edu.cibertec.web.security;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import pe.edu.cibertec.dominio.Usuario;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import pe.edu.cibertec.dominio.UsuarioRol;
import pe.edu.cibertec.dominio.service.UsuarioService;

@Service("userDetailsService")
public class UserSecurityDetailService implements UserDetailsService {

    @Autowired
    private UsuarioService usuarioService;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        Usuario user = usuarioService.buscarUsuarioPorUsuario(username);      
        List<GrantedAuthority> authorities = buildUserAuthority(user.getUsuarioRol());
        return buildUserForAuthentication(user, authorities);
    }

    private User buildUserForAuthentication(Usuario user, List<GrantedAuthority> authorities) {
        return new User(
            user.getUsuario(), 
            user.getClave(),
            user.isEnabled(), 
            true, 
            true, 
            true, 
            authorities
        );
    }

    private List<GrantedAuthority> buildUserAuthority(Set<UsuarioRol> userRoles) {
        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
        for (UsuarioRol userRole : userRoles) {
            setAuths.add(new SimpleGrantedAuthority(userRole.getRol()));
        }
        List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);
        return Result;
    }
}
