package pe.edu.cibertec.web.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler {
    
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest hsr, HttpServletResponse hsr1, Authentication a) throws IOException, ServletException {
        //Usuario usuario = (Usuario)a.getDetails();
        //if (/* verificar si la contraseña del usuario ya tiene más de 90 de días sin cambiar */ true) {
        redirectStrategy.sendRedirect(hsr, hsr1, "/productos.xhtml");
        //hsr1.sendRedirect("http://miserver/miapp/cambiaContrasenaInseguro.do");
        //}
    }
    
}
